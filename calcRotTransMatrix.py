import numpy as np
import numpy.linalg
import numpy.random
import math

def calcRotTransMatrix(p,q):
	
	"""
		M(2,2) * p(2,n)
	"""
	n = p.shape[1]
	
	meanP = np.array((2,1),dtype=float)
	meanQ = np.array((2,1),dtype=float)
	
	
	for x in range(0,n-1):
		meanP += p[:,x]
		meanQ += q[:,x]
	
	meanP /= n
	meanQ /= n
	
	centeredP = np.zeros(p.shape,dtype=float)
	centeredQ = np.zeros(p.shape,dtype=float)
	

	for x in range(0,n-1):
		
		centeredP[:,x] = p[:,x] - meanP
		centeredQ[:,x] = q[:,x] - meanQ
	
		
	X = np.transpose(centeredP)
	Y = centeredQ

	

	S = np.dot(Y,X)
	
	U, sigma, V = np.linalg.svd(S)
	
		
	extI =  np.eye(2)
	#extI[1][1] = np.linalg.det(np.dot(V,np.transpose(U)))
	
	R = np.dot(U,extI).dot(V)
	
	
	T = meanQ - np.dot(R,meanP)
	
	return (R,T)
"""
n = 1000
angle = np.pi/3


sample = np.random.random_sample((3, n))
sample *= 100
sample[2] = np.ones((1,n),dtype=float)




sampleTransform = np.dot(np.array([[math.cos(angle),-math.sin(angle),100],
									  [math.sin(angle),math.cos(angle),150],
									  [0,0,1]]),sample)





R,T = calcRotTransMatrix(np.resize(sample,(2,n)),np.resize(sampleTransform,(2,n)))
Mat = np.zeros((3,3),dtype=float)
Mat[0:2,0:2] = R
Mat[0:2,2] = T 
Mat[2][2] = 1

print(Mat)
print("rot "+str(math.acos(Mat[0][0])-angle))



print(np.sqrt((np.dot(Mat,sample) - sampleTransform) ** 2).mean())
"""
