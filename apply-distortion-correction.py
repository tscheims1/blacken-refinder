"""

	add camera matrix and the distortion coeffients to given images
	Author: James Mayr
"""

import argparse
import imutils
import cv2
import numpy as np
import glob
from os.path import basename
import os

ap = argparse.ArgumentParser()
ap.add_argument('files', type=str, nargs='+',
                    help='files to apply camera matrix and dist coeffs')
args = vars(ap.parse_args())



# the calculated camera matrix and the distortion coeffients
cameraMatrix = np.array([[2.3305833224540861e+03,0,2000],
				[0,2.3305833224540861e+03,1500],
				[0,0,1]]);
distCoeffs = np.array([-1.8986521106837025e-02, 5.5516886170518538e-02, 0., 0.,-5.6624807059392081e-02])

map1 = np.array([])
map2 = np.array([])



#create output dirs, if they dont exists
dir1 = './dist1/'
dir2 = './dist2/'

if ( not os.path.isdir(dir1) ):
	os.makedirs(dir1)
if (not os.path.isdir(dir2)):
	os.makedirs(dir2)

#apply camera matrix and distortion
for imgName in args['files']:
	img  = cv2.imread(imgName)
	#
	imgSize = (img.shape[1],img.shape[0])
	
	
	#two different methods from open cv are used
	newCameraMatrix = cv2.getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imgSize,1)

	maps  = cv2.initUndistortRectifyMap(cameraMatrix, distCoeffs, np.array([[1,0,0],[0,1,0],[0,0,1]])
				,newCameraMatrix[0],
				imgSize, cv2.CV_16SC2, map1, map2)


	img2 = cv2.undistort(img, cameraMatrix, distCoeffs)

	img3 = cv2.remap(img, maps[0], maps[1], cv2.INTER_LINEAR)
	
	
	cv2.imwrite(dir1+str(basename(imgName)),img2)
	cv2.imwrite(dir2+str(basename(imgName)),img3)
	
