import cv2
import numpy as np
from calcRotTransMatrix import calcRotTransMatrix
class FeatureDetectorAzake:
	
	def __init__(self,image,ratio):
		self.image = image
		self.ratio = ratio
		
		
	def getFeatuesAndKeypoints(self):
		descriptor = cv2.xfeatures2d.SIFT_create()
		(kps, features) = descriptor.detectAndCompute(self.image, None)
		kps = np.float32([kp.pt for kp in kps])	
		
		return kps,features
	
	def matchKeypoints(self, kpsA, kpsB, featuresA, featuresB):
		# compute the raw matches and initialize the list of actual
		# matches
		matcher = cv2.DescriptorMatcher_create("BruteForce")
		rawMatches = matcher.knnMatch(featuresA, featuresB, 2)
		matches = []

		
		
		# loop over the raw matches
		for m in rawMatches:
			# ensure the distance is within a certain ratio of each
			# other (i.e. Lowe's ratio test)
			if len(m) == 2 and m[0].distance < m[1].distance * self.ratio:
				matches.append((m[0].trainIdx, m[0].queryIdx))

		
		if len(matches) > 4:
			# construct the two sets of points
			ptsA = np.float32([kpsA[i] for (_, i) in matches])
			ptsB = np.float32([kpsB[i] for (i, _) in matches])
			
			"""avg = np.mean(ptsB-ptsA,axis=0)
			
			H = np.eye(3,dtype=float)
			H[0][2] = avg[0]
			H[1][2] = avg[1]
			"""
			"""R,T= calcRotTransMatrix(np.transpose(ptsA),np.transpose(ptsB))
			
			
			H = np.zeros((3,3),dtype=float)
			H[0:2,0:2] = R
			H[0:2,2] = T
			H[2][2] = 1
			"""
			H = cv2.estimateRigidTransform(ptsA, ptsB,False)
			"""
			#(H, status) = cv2.findHomography(ptsA, ptsB, cv2.RANSAC,4.0)
			"""
			return matches,H
		return matches,None
