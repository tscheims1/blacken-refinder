import math
class ImageDistanceCalculator:
	
	def __init__(self):
		self.fovDiag = math.radians(94)
		self.imageSize = (4000,3000)
	
	def getPixelPerMeter(self,height):
		
		z = height/math.cos(self.fovDiag/2)
		diagInMeter = math.sqrt(z*z- height*height)*2
		
		diagInPixel = math.sqrt(self.imageSize[0]*self.imageSize[0] + self.imageSize[1]*self.imageSize[1])
		
		return diagInPixel/diagInMeter
	
	

		
		

imageDistCalculator = ImageDistanceCalculator()
print(imageDistCalculator.getPixelPerMeter(0.51))
print(imageDistCalculator.getPixelPerMeter(1.4))
print(imageDistCalculator.getPixelPerMeter(0.5))
print(imageDistCalculator.getPixelPerMeter(2))
