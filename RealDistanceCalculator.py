"""
	Utility class for Calculation 
	the distance in Pixels between all Red Points in an image
	(used for accuracy tests)
	Author: James Mayr
	
"""

import glob
import cv2
import numpy as np
import argparse

class RealDistanceCalculator:
	def __init__(self,folder):
		
		self.fileNames = [img for img in glob.glob(folder+'*png')]
		
	def calculatePixelDistance(self):
		for fileName in self.fileNames:
			image = cv2.imread(fileName)
			redPoints = self.findMarkedPixels(image)
			imageGray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			
			print(fileName+";;")
			for i in range(len(redPoints)):

				for j in reversed(range(i+1,len(redPoints))):
					dist = np.linalg.norm(redPoints[i] - redPoints[j])
					print(str(redPoints[i])+";"+str(redPoints[j])+";"+str(dist))
			
			
			
			
			
		
	def findMarkedPixels(self,image):
		
		#image is in BGR-Format
		R = image[:,:,2]
		locR = np.where(R==255)

		redPoints = []
		for i in range(0,len(locR[0])):
			x = locR[0][i]
			y = locR[1][i]
			if(image[x,y,1] == 0 and image[x,y,0] ==0):
				redPoints.append(np.array([x,y]))

		return redPoints
		
if __name__ == '__main__':
	ap = argparse.ArgumentParser()
	ap.add_argument('imageFolder',type=str)

	
	args = vars(ap.parse_args())
		
	realDistanceCalculator = RealDistanceCalculator(args['imageFolder'])
	realDistanceCalculator.calculatePixelDistance()
