""" 
	Just a Test-Interface for the Image-Finder
	Author: James Mayr
"""


from ImageFinder import ImageFinder
import cv2
import imutils
from FeatureDetectorAkaze import FeatureDetectorAkaze

image = cv2.imread('DJI_0893.JPG')


image = imutils.resize(image,width=1000)

featureDetector = FeatureDetectorAkaze(image,0.75)
keyPoints,features = featureDetector.getFeatuesAndKeypoints()


position = (0.73,0.425)
size = (150,150)
imageFinder = ImageFinder(image,position,size,keyPoints,features)

imageFinder.findImageOnVideo('DJI_0896.MP4',25)
