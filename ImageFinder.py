""" 
	This class is responsible for refinding
	a given image in video stream
	Author: James Mayr
"""


import cv2
import imutils
import numpy as np
from FeatureDetector import FeatureDetector
import math
import time
from FeatureDetectorAkaze import FeatureDetectorAkaze
count =0
class ImageFinder:
	
	def __init__(self,image,position,size,keyPoints,features):
		self.image = image
		self.keyPoints = keyPoints
		self.features = features
		self.position = position
		self.size = size
		self.count = 0
	def findImageOnVideo(self,videoFile,framerate):
		
		cap = cv2.VideoCapture(videoFile)
		
		timeList = []
		fourcc = cv2.VideoWriter_fourcc(*'XVID')
		out = cv2.VideoWriter('output.avi',fourcc, framerate, ( 1000,562))
		while(cap.isOpened()):
			
			frameId = cap.get(1) #current frame number
			ret, frame = cap.read()
		
			if(ret == False):
				break
			
			if (frameId % math.floor(min(1,int(25/framerate))) == 0):
				
				frame = imutils.resize(frame,width=1000)
				start = time.time()
				featureDetector = FeatureDetectorAkaze(frame,0.75)
				keyPoints, features= featureDetector.getFeatuesAndKeypoints()
				
				matches,H = featureDetector.matchKeypoints(self.keyPoints,keyPoints,self.features,features)
				end = time.time()
				print(str(end-start)+";"+str(len(matches)))
				timeList.append(end-start)
				self.count+=1
				
				if(H is not None):
					newPoint = np.dot(H,np.array([self.position[1]* self.image.shape[1],self.position[0]*self.image.shape[0],1]))
					newPointX = int(newPoint[0])
					newPointY = int(newPoint[1])
					frame = cv2.rectangle(frame,(newPointX - self.size[1]/2,newPointY - self.size[0]/2),(newPointX + self.size[1]/2, newPointY+ self.size[0]/2),(0,0,255),2)
					
					out.write(frame.astype('uint8'))
	
					
				else:
					out.write(frame.astype('uint8'))
		
		
		cap.release()
		out.release()
		print("avg")
		print(sum(timeList)/float(len(timeList)))
				
	def drawMatches(self, imageA, imageB, kpsA, kpsB, matches):
		imageA = imutils.resize(imageA,width=1000)
	
		
		(hA, wA) = imageA.shape[:2]
		(hB, wB) = imageB.shape[:2]
		vis = np.zeros((max(hA, hB), wA + wB, 3), dtype="uint8")
		vis[0:hA, 0:wA] = imageA
		vis[0:hB, wA:] = imageB

		for ((trainIdx, queryIdx)) in matches:

			ptA = (int(kpsA[queryIdx][0]), int(kpsA[queryIdx][1]))
			ptB = (int(kpsB[trainIdx][0]) + wA, int(kpsB[trainIdx][1]))
			cv2.line(vis, ptA, ptB, (255, 0, 0), 1)
			

		return vis
