"""
	Feature detection with the AKAZE-Algorithm
	Author: James Mayr
"""


import cv2
import numpy as np
from calcRotTransMatrix import calcRotTransMatrix
class FeatureDetectorAkaze:
	
	def __init__(self,image,ratio):
		self.image = image
		self.ratio = ratio
		
		
	def getFeatuesAndKeypoints(self):
		detector = cv2.AKAZE_create()
		(kps, features) = detector.detectAndCompute(self.image, None)
		kps = np.float32([kp.pt for kp in kps])	
		
		return kps,features
	
	def matchKeypoints(self, kpsA, kpsB, featuresA, featuresB):
		bf = cv2.BFMatcher(cv2.NORM_HAMMING)
		rawMatches = bf.knnMatch(featuresA,featuresB, k=2)
		
		# loop over the raw matches
		matches = []
		for m,n in rawMatches:
			# ensure the distance is within a certain ratio of each
			# other (i.e. Lowe's ratio test)
			if (m.distance < self.ratio*n.distance):
				matches.append(m)

		if len(matches) > 4:
			# construct the two sets of points
			
			ptsA = np.float32([kpsA[mat.queryIdx] for mat in matches]) 
			ptsB = np.float32([kpsB[mat.trainIdx] for mat in matches])
			H = cv2.estimateRigidTransform(ptsA, ptsB,False)

			return matches,H
		return matches,None
