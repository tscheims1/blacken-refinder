# Blacken refinder

This repository is a collection of scripts for refinding already detected blacken.


## Runing the refinder

just run the following command: `python2 ImageFinderTest.py` and it will create after a few minutes a file named `output.avi`

## Result of the AKAZE detection test

[Video](result.avi)

## Result of the AKAZE and SIFT benchmark

[test-results](test-results)